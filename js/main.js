//

const BASE_URL = "https://63f620359daf59d1ad826785.mockapi.io";

fetchDSSV();
function fetchDSSV() {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      var dssv = res.data;
      turnOffLoading();
      renderDSSV(dssv);
      console.log("res: ", res.data);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function renderDSSV(dssv) {
  // render dssv lên table
  var contentHTML = "";

  for (var i = 0; i < dssv.length; i++) {
    var item = dssv[i];
    contentHTML += `  
        <tr>
            <td> ${item.ma} </td>
            <td> ${item.ten} </td>
            <td> ${item.email} </td>
            <td>0 </td>
            <td> 
            <button onclick="xoaSv('${item.ma}')" class="btn btn-danger"> Xóa </button>
            </td>
            <td> 
            <button onclick="suaSv('${item.ma}')" class="btn btn-warning"> Sửa </button>
            </td>
        </tr>
        `;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function xoaSv(id) {
  console.log("id: ", id);
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      fetchDSSV();
      console.log("res: ", res);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function suaSv(id) {
  console.log("id: ", id);
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      showThongTinLenForm(res.data);
      console.log("res: ", res);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function capNhatSv() {
  var sv = layThongTinTuForm();
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  })
    .then(function (res) {
      turnOffLoading();
      fetchDSSV();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function themSinhVien() {
  var sv = layThongTinTuForm();
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      turnOffLoading();
      fetchDSSV();
      console.log("res: ", res);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
