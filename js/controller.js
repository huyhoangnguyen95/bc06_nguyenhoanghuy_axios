function showThongTinLenForm(object) {
  document.getElementById("txtMaSV").value = object.ma;
  document.getElementById("txtTenSV").value = object.ten;
  document.getElementById("txtEmail").value = object.email;
  document.getElementById("txtPass").value = object.matKhau;
  document.getElementById("txtDiemToan").value = object.toan;
  document.getElementById("txtDiemLy").value = object.ly;
  document.getElementById("txtDiemHoa").value = object.hoa;
}

function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;

  return {
    ma: ma,
    ten: ten,
    email: email,
    matKhau: matKhau,
    toan: toan,
    ly: ly,
    hoa: hoa,
  };

  // return new SinhVien(ma, ten, email, matKhau, toan, ly, hoa);
}

let turnOnLoading = () => {
  document.getElementById("loading").style.display = "flex";
};

let turnOffLoading = () => {
  document.getElementById("loading").style.display = "none";
};
